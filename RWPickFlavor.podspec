Pod::Spec.new do |spec|

  # ―――  Spec Metadata  ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  These will help people to find your library, and whilst it
  #  can feel like a chore to fill in it's definitely to your advantage. The
  #  summary should be tweet-length, and the description more in depth.
  #


  #1
  spec.platform     = :ios, "12.1"
  spec.name         = "RWPickFlavor"
  spec.summary      = "RWPickFlavor lets a user select an ice cream flavor."
  spec.requires_arc    = true

  #2
  spec.version      = "0.1.0"
  
  #3
  
  spec.license = { :type => "MIT", :file => "LICENSE" }
  
  #4
  
  spec.author = {" Marcos Fellipe Costa Silva" => "marcosfellipec@gmail.com" }
  
  #5
  
  spec.homepage = "https://gitlab.com/marcosfellipec/rwpickflavors"
  
  #6
  
  spec.source = { :git => "https://gitlab.com/marcosfellipec/rwpickflavors.git", :tag => "#{spec.version}" }
  
  #7
  
  spec.framework = "UIKit"
  spec.dependency 'Alamofire', '~> 4.7'
  spec.dependency 'MBProgressHUD', '~> 1.1.0'
  
  #8
  
  spec.source_files = "RWPickFlavor/**/*.{swift}"
  
  #9
  
  spec.resources = "RWPickFlavor/**/*.{png, jpeg, jpg,storyboard,xib,xcassets}"
  #10
  
  spec.swift_version = "4.2"
  
  end
